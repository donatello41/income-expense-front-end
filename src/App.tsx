import React from 'react';
import '../node_modules/flowbite/dist/flowbite.js';
import RegisterForm from './components/RegisterForm';
import { Route } from 'react-router-dom';
import { Routes } from 'react-router-dom';
import LoginForm from './components/LoginForm';
import Layout from './components/Layout';
import RequiredAuth from './components/RequiredAuth';
import CreateCategory from './components/Category/CreateCategory';
import PersistLogin from './components/PersistLogin';
import CategoryIndex from './components/Category/CategoryIndex';
import EditCategory from './components/Category/EditCategory';
import IncomeExpenseIndex from './components/IncomeExpense/IncomeExpenseIndex';
import CreateIncomeExpense from './components/IncomeExpense/CreateIncomeExpense';
import EditIncomeExpense from './components/IncomeExpense/EditIncomeExpense';

function App() {
  return (
    <Routes>
        <Route path="/" element={<Layout />}>

            {/* public routes */}
            <Route path="login" element={<LoginForm />} />
            <Route path="register" element={<RegisterForm />} />

            {/* private routes */}
            <Route element={<PersistLogin />}>
                <Route element={<RequiredAuth />}>
                    <Route path="/categories" element={<CategoryIndex />}/>
                    <Route path="/category/create" element={<CreateCategory />}/>
                    <Route path="/category/:categoryId/edit" element={ <EditCategory /> }/>

                    <Route path="/income-expense" element={<IncomeExpenseIndex />}/>
                    <Route path="/income-expense/create" element={<CreateIncomeExpense />}/>
                    <Route path="/income-expense/:recordId/edit" element={ <EditIncomeExpense /> }/>
                </Route>
            </Route>

        </Route>
    </Routes>
  );
}

export default App;
