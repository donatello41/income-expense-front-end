import React, { createContext, useEffect, useState } from 'react';
import useAuth from '../hooks/useAuth';
const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
    const [auth, setAuth] = useState({ });

    if (auth?.user) {
        localStorage.setItem('auth', JSON.stringify(auth))
    }

    return (
        <AuthContext.Provider value={{ auth, setAuth }}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthContext;