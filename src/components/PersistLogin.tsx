import React, { useEffect, useState } from 'react';
import useAuth from '../hooks/useAuth';
import { Outlet } from 'react-router-dom';

const PersistLogin = () => {
    const { setAuth }  = useAuth();
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        let user = JSON.parse(localStorage.getItem('auth') || '{}')
        if (!(Object.keys(user).length === 0 && user.constructor === Object)) {
            setAuth(user);
            setIsLoading(false);
        }else {
            setIsLoading(true);
        }
    }, [isLoading])
    return (
        <div>
            { isLoading ? <p>Lütfen giriş yapınız</p> : <Outlet /> }
        </div>
    )
}

export default PersistLogin;