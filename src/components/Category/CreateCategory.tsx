import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import useAxiosPrivate from '../../hooks/useAxiosPrivate';
import * as yup from 'yup';

const CREATE_CATEGORY_URL = 'category/create';

const validationSchema = yup.object({
    name: yup.string().required("İsim Alanı Gereklidir"),
    type: yup.string().required("Kategori Bilgisi gereklidir"),
});

const CreateCategory = () => {

    const navigate = useNavigate();
    const from = "/categories";
    const axiosPrivate = useAxiosPrivate();

    const [success, setSuccess] = useState(null);
    const [error, setError] = useState(null );

    const onSubmit = async (data: any) => {
        try {
            const response = await axiosPrivate.post(CREATE_CATEGORY_URL, data);
            navigate(from, {replace:true});
            setSuccess(response.data.message);
        } catch (err) {
            if (err.response) {
                setError(err.response.date.message)
            }
        }
    }

    const formik = useFormik({
        initialValues: {
            name: "",
            type: "INCOME",
        },
        validateOnBlur: true,
        onSubmit,
        validationSchema: validationSchema,
    });

    return (
        <div>
            <div>
                <div className="grid grid-cols-12">
                    <div className="col-start-6 col-span-3">
                        <p className="mt-2 text-sm text-red-600 dark:text-red-500 mb-6">
                            { error ? error : '' }
                        </p>
                        <p className="mt-2 text-sm text-green-600 dark:text-green-500 mb-6">
                            { success ? success : '' }
                        </p>
                        <form onSubmit={formik.handleSubmit}>
                            <div className="mb-6">
                                <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                    Kategori Adı
                                </label>
                                <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                                    {formik.touched.name && formik.errors.name ? formik.errors.name : ''}
                                </p>
                                <input
                                    type="name"
                                    id="name"
                                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder=""
                                    autoComplete={'off'}
                                    value={formik.values.name}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                />
                            </div>
                            <div className="mb-6">
                                <label htmlFor="type"
                                       className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Lütfen kategori bilgisi seçiniz</label>
                                <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                                    {formik.touched.type && formik.errors.type ? formik.errors.type : ''}
                                </p>
                                <select
                                        id="type"
                                        value={formik.values.type}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option value="INCOME">Gelir</option>
                                    <option value="EXPENSE">Gider</option>
                                </select>
                            </div>
                            <button
                                type="submit"
                                className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            >
                                Kategori Oluştur
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CreateCategory;