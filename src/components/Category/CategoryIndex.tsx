import React, { useEffect, useState } from 'react';
import useAxiosPrivate from '../../hooks/useAxiosPrivate';
import { Link } from 'react-router-dom';

const GET_CATEGORY_URL = 'categories';
const DELETE_CATEGORY_URL = 'category/';

const CategoryIndex = () => {
    const [categories, setCategories] = useState([]);
    const [success, setSuccess] = useState(null);
    const [error, setError] = useState(null );
    const axiosPrivate = useAxiosPrivate();

    useEffect(() => {
        fetchCategories();
    }, []);

    const fetchCategories = async () => {
        try {
            const response = await axiosPrivate.get(GET_CATEGORY_URL);
            setCategories(response.data.data);
            setSuccess(response.data.data.message);
        } catch (err) {
            if (!err.response) {
                setError(err.response.data.message)
            }
        }
    }

    const deleteCategory = async (categoryId) => {
        try {
            const response = await axiosPrivate.delete(DELETE_CATEGORY_URL + categoryId + '/delete' );
            setCategories(response.data.data);
            fetchCategories();
        } catch (err) {
            if (err.response) {
                setError(err.response.data.message)
            }
        }
    }

    return (
        <div>
            <div className="grid grid-cols-12 content-center items-center">
                <h1 className="col-span-12 text-center">Kategoriler</h1>
                <p className="mt-2 text-sm text-red-600 dark:text-red-500 mb-6">
                    { error ? error : '' }
                </p>
                <p className="mt-2 text-sm text-green-600 dark:text-green-500 mb-6">
                    { success ? success : '' }
                </p>
                <div className="col-span-2 col-start-2 mt-4">
                    <Link to={"/category/create"} >
                        <a
                            type="button"
                            className="text-white bg-green-700 hover:cursor-pointer hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">
                            Kategori Oluştur
                        </a>
                    </Link>
                </div>
                <div className="col-span-8 mt-1 col-start-2">
                    <div className="flex flex-col">
                        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
                            <div className="inline-block py-2 min-w-full sm:px-6 lg:px-8">
                                <div className="overflow-hidden shadow-md sm:rounded-lg">
                                    <table className="min-w-full">
                                        <thead className="bg-gray-100 dark:bg-gray-700">
                                            <tr>
                                                <th scope="col"
                                                    className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    Kategori Adı
                                                </th>
                                                <th scope="col"
                                                    className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    Kategori Türü
                                                </th>
                                                <th scope="col" className="relative py-3 px-6">
                                                    <span className="sr-only">Düzenle</span>
                                                    <span className="sr-only">Sil</span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            categories.map(category => (
                                                <tr key = {category.id}
                                                    className="border-b odd:bg-white even:bg-gray-50 odd:dark:bg-gray-800 even:dark:bg-gray-700 dark:border-gray-600">
                                                    <td className="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                                        {category.name}
                                                    </td>
                                                    <td className="py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                                        {category.type}
                                                    </td>
                                                    <td className="py-4 px-6 text-sm font-medium text-right whitespace-nowrap">
                                                    <a href="#"
                                                    className="text-blue-600 hover:underline">
                                                        <Link to={`/category/${category.id}/edit`}>Düzenle</Link>
                                                    </a>

                                                    <a onClick={() => deleteCategory(category.id)}
                                                        href="#"
                                                        className="text-red-600 hover:underline ml-4">Sil</a>
                                                    </td>
                                                    </tr>
                                            ))
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CategoryIndex;