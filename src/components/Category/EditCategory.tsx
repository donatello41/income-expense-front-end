import React, { useEffect, useState } from 'react';
import { Formik, Field,Form } from 'formik';
import { useNavigate, useParams } from 'react-router-dom';
import useAxiosPrivate from '../../hooks/useAxiosPrivate';
import * as yup from 'yup';

const CATEGORY_URL = 'category/';

const validationSchema = yup.object({
    name: yup.string().required("İsim Alanı Gereklidir"),
    type: yup.string().required("Kategori Bilgisi gereklidir"),
});

interface Category {
    name: 'string',
    type: 'string'
}

const EditCategory = () => {
    const navigate = useNavigate();
    const from = "/categories";
    let { categoryId } = useParams();
    const [category, setCategory] = useState(null);
    const [success, setSuccess] = useState(null);
    const axiosPrivate = useAxiosPrivate();

    useEffect(() => {
        fetchCategory(categoryId);
    }, [categoryId]);

    const [error, setError] = useState(null );

    const fetchCategory = async (categoryId: number) => {
        try {
            let category_id = {'category_id': Number(categoryId)};
            const { data } = await axiosPrivate.post(CATEGORY_URL + "/" , category_id);
            setCategory(data.data );
        } catch (err) {
            if (err.response) {
                setError(err.response.data.message)
            }
        }
    }

    const handleSubmit = async (formData: any) => {
        try {
            const { data } = await axiosPrivate.post(CATEGORY_URL + category.id + '/update', formData);
            setSuccess(data.message);
            navigate(from, {replace:true});
        } catch (err) {
            if (err?.response?.data?.message) {
                setSuccess(null);
                setError(err.response.data.message)
            }
        }
    }

    return (
        <div>
            <div className="grid grid-cols-12">
                <div className="col-start-6 col-span-3">
                     <p className="mt-2 text-sm text-red-600 dark:text-red-500 mb-6">
                        { error ? error : '' }
                    </p>
                    <p className="mt-2 text-sm text-green-600 dark:text-green-500 mb-6">
                        { success ? success : '' }
                    </p>
                    {category &&
                        <Formik
                            initialValues={{ name: category.name, type: category.type }}
                            onSubmit={handleSubmit}
                            validationSchema={validationSchema}
                            validateOnBlur={true}>
                            <Form>
                                <div className="mb-6">
                                    <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                        Kategori Adı
                                    </label>
                                    <Field type="text" name="name" placeholder="Kategori Adı" >
                                        {({
                                            field,
                                            form: { touched , errors },
                                            meta,
                                        }) => (
                                            <div>
                                                <input className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" type="text" placeholder="Kategori Adı" {...field} />
                                                {meta.touched && meta.error && (
                                                    <div className="mt-2 text-sm text-red-600 dark:text-red-500">{meta.error}</div>
                                                )}
                                            </div>
                                        )}
                                    </Field>
                                </div>
                                <div className="mb-6">
                                    <label htmlFor="type"
                                        className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Lütfen kategori türü seçiniz</label>
                                    <p className="mt-2 text-sm text-red-600 dark:text-red-500">

                                    </p>
                                    <Field as="text" name="type" placeholder="" >
                                        {({
                                              field,
                                              form: { touched , errors },
                                              meta,
                                          }) => (
                                            <div>
                                                {meta.touched && meta.error && (
                                                    <div className="mt-2 text-sm text-red-600 dark:text-red-500">{meta.error}</div>
                                                )}
                                                <select
                                                    {...field}
                                                    id="type"
                                                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                                        <option value="INCOME">Gelir</option>
                                                        <option value="EXPENSE">Gider</option>
                                                </select>
                                                {meta.touched && meta.error && (
                                                    <div className="mt-2 text-sm text-red-600 dark:text-red-500">{meta.error}</div>
                                                )}
                                            </div>
                                        )}
                                    </Field>
                                </div>
                                <button
                                    type="submit"
                                    className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                >
                                    Güncelle
                                </button>
                            </Form>
                        </Formik>
                    }
                </div>
            </div>
        </div>
    );
}

export default EditCategory;