import axios from 'axios';
import React, { useState } from 'react';
import * as yup from 'yup';
import { useFormik } from 'formik';

const validationSchema = yup.object({
  name: yup.string().min(3, "Lütfen gerçek adınızı soyadınızı girin").required("Adınız ve Soyadınız gereklidir"),
  email: yup.string().email().required("E-posta adresiniz gereklidir"),
  password: yup.string().required("Şifre gereklidir"),
  password_confirmation: yup.string().when("password", {
    is: (val: string | any[]) => (!!(val && val.length > 6)),
    then: yup.string().oneOf([yup.ref("password")], "Şifreler eşleşmiyor")
  }),
});

const RegisterForm = () => {
  const [success, setSuccess] = useState(null);
  const [error, setError] = useState(null);

  const onSubmit = async (data: any) => {
    const response = await axios.post('http://localhost:8000/api/register', data).catch((err) => {
      if (err && err.response) {
        setSuccess(null);
        setError(err.response.data.message);
      }
    });
    if (response && response.data) {
      setSuccess(response.data.message);
      formik.resetForm();
    }
  }

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      password: "",
      password_confirmation: ""
    },
    validateOnBlur: true,
    onSubmit,
    validationSchema: validationSchema,
  });

  return (
    <div>
      <div className="grid grid-cols-12">
        <div className="col-start-6 col-span-3">
          <p className="mt-2 text-sm text-red-600 dark:text-red-500">
            { error ? error : '' }
          </p>
          <p className="mt-2 text-sm text-green-600 dark:text-green-500">
            { success ? success : '' }
          </p>
          <form onSubmit={formik.handleSubmit}>
            <div className="relative z-0 mb-6 w-full group">
              <input
                type="text"
                name="name"
                id="name"
                className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                placeholder=""
                autoComplete={'off'}
                value={formik.values.name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
              <label
                htmlFor="name"
                className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
              >
                Ad Soyad
              </label>
              <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                {formik.touched.name && formik.errors.name ? formik.errors.name : ''}
              </p>
            </div>
            <div className="relative z-0 mb-6 w-full group">
              <input
                value={formik.values.email}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                type="email"
                name="email"
                className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                placeholder=" "
                autoComplete={'off'}
              />
              <label
                htmlFor="email"
                className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
              >
                E-posta Adresi
              </label>
              <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                {formik.touched.email && formik.errors.email ? formik.errors.email : ''}
              </p>
            </div>
            <div className="relative z-0 mb-6 w-full group">
              <input
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                type="password"
                name="password"
                id="password"
                className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                placeholder=" "
                autoComplete={'off'}
              />
              <label
                htmlFor="password"
                className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
              >
                Şifre
              </label>
              <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                {formik.touched.password && formik.errors.password ? formik.errors.password : ''}
              </p>
            </div>
            <div className="relative z-0 mb-6 w-full group">
              <input
                value={formik.values.password_confirmation}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                type="password"
                name="password_confirmation"
                id="password_confirmation"
                className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                placeholder=" "
                autoComplete={'off'}
              />
              <label
                htmlFor="password_confirmation"
                className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
              >
                Şifre Tekrar
              </label>
              <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                {formik.touched.password_confirmation && formik.errors.password_confirmation ? formik.errors.password_confirmation : ''}
              </p>
            </div>
            <button
              type="submit"
              className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Kayıt Ol
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default RegisterForm;
