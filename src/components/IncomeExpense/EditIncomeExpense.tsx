import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Field, Form, Formik } from 'formik';
import useAxiosPrivate from '../../hooks/useAxiosPrivate';
import * as yup from 'yup';

const GET_INCOME_EXPENSE_URL = 'income-expense-record';
const EDIT_INCOME_EXPENSE_URL = 'income-expense/';
const GET_CATEGORY_URL = 'categories';

const validationSchema = yup.object({
    transaction_date: yup.date().required("Tarih alanı gereklidir"),
    amount: yup.number().required("Miktar alanı gereklidir"),
    category_id: yup.number().required("Kategori alanı gereklidir"),
    currency: yup.string().required("Miktar alanı gereklidir"),
});

const EditIncomeExpense = () => {

    const navigate = useNavigate();
    const from = "/income-expense";
    let { recordId } = useParams();
    const [categories, setCategories] = useState([]);
    const [record, setRecord] = useState(null);

    const axiosPrivate = useAxiosPrivate();

    const [success, setSuccess] = useState(null);
    const [error, setError] = useState(null );

    useEffect(() => {
        fetchCategories();
        fetchRecord(recordId);
    }, [recordId]);


    const fetchCategories = async () => {
        try {
            const response = await axiosPrivate.get(GET_CATEGORY_URL);
            setCategories(response.data.data);
            setSuccess(response.data.data.message);
        } catch (err) {
            if (!err.response) {
                setError(err.response.data.message)
            }
        }
    }

    const fetchRecord = async (recordId: number) => {
        try {
            let record_id = {'id': Number(recordId)};
            const response = await axiosPrivate.post(GET_INCOME_EXPENSE_URL, record_id);
            setRecord(response.data.data);
            console.log(response.data.data);
            setSuccess(response.data.data.message);
        } catch (err) {
            if (err.response) {
                setError(err.response.data.message)
            }
        }
    }

    const padTo2Digits = (num) => {
        return num.toString().padStart(2, '0');
    }

    const formatDate = (date) => {
        date = new Date(date);
        let newDate = [
            date.getFullYear(),
            padTo2Digits(date.getDate()),
            padTo2Digits(date.getMonth() + 1),
        ].join('-');
        return newDate;
    }

    const handleSubmit = async (formData: any) => {
        try {
            const response = await axiosPrivate.post(EDIT_INCOME_EXPENSE_URL + record.id + '/update', formData);
            setSuccess(response.data.message);
            navigate(from, {replace:true});
        } catch (err) {
            if (err.response) {
                setError(err.response.data.message)
            }
        }
    }

    return (
        <div>
            <div className="grid grid-cols-12">
                <div className="col-start-6 col-span-3">
                    <p className="mt-2 text-sm text-red-600 dark:text-red-500 mb-6">
                        { error ? error : '' }
                    </p>
                    <p className="mt-2 text-sm text-green-600 dark:text-green-500 mb-6">
                        { success ? success : '' }
                    </p>
                    {record &&
                        <Formik
                            initialValues={{transaction_date: formatDate(record.transaction_date), amount: record.amount, currency: record.currency, category_id: record.income_expense_category_id, description: record.description}}
                            onSubmit={handleSubmit}
                            validationSchema={validationSchema}
                            validateOnBlur={true}>
                            <Form>
                                <div className="mb-6">
                                    <label htmlFor="type"
                                           className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Lütfen kategori seçiniz</label>
                                    <p className="mt-2 text-sm text-red-600 dark:text-red-500">

                                    </p>
                                    <Field name="category_id" >
                                        {({
                                              field,
                                              form: { touched , errors },
                                              meta,
                                          }) => (
                                            <div>
                                                <select
                                                    {...field}
                                                    name="category_id"
                                                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                                    <option></option>
                                                    { categories &&
                                                        categories.map( category => (
                                                            <option value={category.id}>
                                                                { category.name }
                                                            </option>
                                                        ))
                                                    }
                                                </select>
                                                {meta.touched && meta.error && (
                                                    <div className="mt-2 text-sm text-red-600 dark:text-red-500">{meta.error}</div>
                                                )}
                                            </div>
                                        )}
                                    </Field>
                                </div>
                                <div className="mb-6">
                                    <label htmlFor="transaction_date">İşlem Tarihi:</label>
                                    <Field
                                        name="transaction_date">
                                        {({
                                              field,
                                              form: { touched , errors },
                                              meta,
                                          }) => (
                                            <div>
                                                <input
                                                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                    type="date"
                                                    name="transaction_date"
                                                    placeholder="Miktar Giriniz"
                                                    {...field} />
                                                {meta.touched && meta.error && (
                                                    <div className="mt-2 text-sm text-red-600 dark:text-red-500">{meta.error}</div>
                                                )}
                                            </div>
                                        )}
                                    </Field>
                                </div>
                                <div className="mb-6">
                                    <label htmlFor="number" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                                        Miktar
                                    </label>
                                    <Field type="text" name="amount">
                                        {({
                                              field,
                                              form: { touched , errors },
                                              meta,
                                          }) => (
                                            <div>
                                                <input
                                                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                    type="number"
                                                    placeholder="Miktar Giriniz"
                                                    {...field} />
                                                {meta.touched && meta.error && (
                                                    <div className="mt-2 text-sm text-red-600 dark:text-red-500">{meta.error}</div>
                                                )}
                                            </div>
                                        )}
                                    </Field>
                                </div>
                                <div className="mb-6">
                                    <label htmlFor="type"
                                           className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Lütfen para birimi seçiniz</label>
                                    <p className="mt-2 text-sm text-red-600 dark:text-red-500">

                                    </p>
                                    <Field name="currency">
                                        {({
                                              field,
                                              form: { touched , errors },
                                              meta,
                                          }) => (
                                            <div>
                                                {meta.touched && meta.error && (
                                                    <div className="mt-2 text-sm text-red-600 dark:text-red-500">{meta.error}</div>
                                                )}
                                                <select
                                                    {...field}
                                                    name="currency"
                                                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                                    <option/>
                                                    <option value="TRY">TRY</option>
                                                    <option value="EUR">EUR</option>
                                                    <option value="USD">USD</option>
                                                </select>
                                                {meta.touched && meta.error && (
                                                    <div className="mt-2 text-sm text-red-600 dark:text-red-500">{meta.error}</div>
                                                )}
                                            </div>
                                        )}
                                    </Field>
                                </div>
                                <div className="mb-6">
                                    <label htmlFor="message" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">
                                        Açıklama
                                    </label>
                                    <Field name="description">
                                        {({
                                              field,
                                              form: { touched , errors },
                                              meta,
                                          }) => (
                                            <div>
                                                <textarea
                                                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                    type="textarea"
                                                    placeholder="Açıklama Giriniz"
                                                    {...field} />
                                                {meta.touched && meta.error && (
                                                    <div className="mt-2 text-sm text-red-600 dark:text-red-500">{meta.error}</div>
                                                )}
                                            </div>
                                        )}
                                    </Field>
                                </div>
                                <button
                                    type="submit"
                                    className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                >
                                    Güncelle
                                </button>
                            </Form>
                        </Formik>
                    }
                </div>
            </div>
        </div>
    );
}

export default EditIncomeExpense;