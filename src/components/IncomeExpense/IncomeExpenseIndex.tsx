import React, { useEffect, useState } from 'react';
import useAxiosPrivate from '../../hooks/useAxiosPrivate';
import { Link } from 'react-router-dom';

const GET_INCOME_EXPENSE_URL = 'income-expense';
const DELETE_CATEGORY_URL = 'income-expense/';
const GET_CATEGORY_URL = 'category/';

const IncomeExpenseIndex = () => {
    const [records, setRecords] = useState(null);
    const [success, setSuccess] = useState(null);
    const [error, setError] = useState(null );
    const axiosPrivate = useAxiosPrivate();

    const fetchRecords = async () => {
        try {
            const { data } = await axiosPrivate.get(GET_INCOME_EXPENSE_URL);
            setRecords(data.data);
            setSuccess(data.message);
        } catch (err) {
            if (!err.response) {
                setError(err.response.data.message)
            }
        }
    };

    useEffect(() => {
        fetchRecords();
    }, []);

    const setCategoryName = (items: any[]) => {
        let recordsWithCategoryName: any[] = [];
        items.map( item => {
             getCategoryName(item.income_expense_category_id).then(response => {
                 item.income_expense_category_id = response.data.data.name;
                 recordsWithCategoryName.push(item);
             });
        });
        return recordsWithCategoryName;
    }

    const getCategoryName = async (categoryId) => {
        let category_id = {'category_id': Number(categoryId)};
        return await axiosPrivate.post(GET_CATEGORY_URL + "/", category_id)
    }

    const deleteRecord = async (recordId) => {
        try {
            const response = await axiosPrivate.delete(DELETE_CATEGORY_URL + recordId + '/delete' );
            console.log(response);
            fetchRecords();
        } catch (err) {
            if (!err.response) {
                setError(err.response.message)
            }
        }
    }

    return (
        <div>
            <div className="grid grid-cols-12 content-center items-center">
                <h1 className="col-span-12 text-center">Kategoriler</h1>
                <div className="col-span-2 col-start-2 mt-4">
                    <Link to={"/income-expense/create"} >
                        <a
                            type="button"
                            className="text-white bg-green-700 hover:cursor-pointer hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">
                             Yeni Gelir Gider Oluştur
                        </a>
                    </Link>
                </div>
                {records &&
                    <div className="col-span-8 mt-1 col-start-2">
                        <div className="flex flex-col">
                            <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
                                <div className="inline-block py-2 min-w-full sm:px-6 lg:px-8">
                                    <div className="overflow-hidden shadow-md sm:rounded-lg">
                                        <table className="min-w-full">
                                            <thead className="bg-gray-100 dark:bg-gray-700">
                                            <tr>
                                                <th scope="col"
                                                    className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    Gelir Gider Kategorisi
                                                </th>
                                                <th scope="col"
                                                    className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    İşlem Tarihi
                                                </th>
                                                <th scope="col"
                                                    className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    Tutar
                                                </th>
                                                <th scope="col"
                                                    className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    Para Birimi
                                                </th>
                                                <th scope="col"
                                                    className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    Açıklama
                                                </th>
                                                <th scope="col" className="relative py-3 px-6">
                                                    <span className="sr-only">Düzenle</span>
                                                    <span className="sr-only">Sil</span>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                records.map(record => (
                                                    <tr key={record.id}
                                                        className="border-b odd:bg-white even:bg-gray-50 odd:dark:bg-gray-800 even:dark:bg-gray-700 dark:border-gray-600">
                                                        <td className="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                                            {record.income_expense_category_id}
                                                        </td>
                                                        <td className="py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                                            {record.transaction_date}
                                                        </td>
                                                        <td className="py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                                            {record.amount}
                                                        </td>
                                                        <td className="py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                                            {record.currency}
                                                        </td>
                                                        <td className="py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                                            {record.description}
                                                        </td>
                                                        <td className="py-4 px-6 text-sm font-medium text-right whitespace-nowrap">
                                                            <a href="#"
                                                               className="text-blue-600 hover:underline">
                                                                <Link
                                                                    to={`/income-expense/${record.id}/edit`}>Düzenle</Link>
                                                            </a>

                                                            <a onClick={() => deleteRecord(record.id)}
                                                               className="text-red-600 hover:underline ml-4 hover:cursor-pointer">Sil</a>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </div>
    );
}

export default IncomeExpenseIndex;