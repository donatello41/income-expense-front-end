import React from 'react';
import { useLocation, Navigate, Outlet } from 'react-router-dom';
import useAuth from "../hooks/useAuth";

const RequiredAuth = () => {
    const { auth } = useAuth();
    const location = useLocation();

    return(
        auth?.user ? <Outlet /> : <Navigate to={"/login"} state={{location}} replace />
    )
}

export default RequiredAuth;