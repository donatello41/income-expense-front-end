import axios from '../api/axios';
import React, { useContext, useState } from 'react';
import * as yup from 'yup';
import { useFormik } from 'formik';
/*import AuthContext from '../context/AuthProvider';*/
import useAuth from '../hooks/useAuth';
import { useNavigate, useLocation, Link } from 'react-router-dom';

const LOGIN_URL = 'login';

const validationSchema = yup.object({
  email: yup.string().email("Lütfen geçerli bir mail girin").required("E-posta adresiniz gereklidir"),
  password: yup.string().required("Şifre gereklidir"),
});

const LoginForm = () => {
  const { setAuth } = useAuth();

  const navigate = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathname || "/dashboard";

  const [success, setSuccess] = useState(null);
  const [error, setError] = useState(null );

  const onSubmit = async (data: any) => {
    console.log(data);
    try {
      const response = await axios.post(LOGIN_URL, data);
      const accessToken = response?.data?.accessToken;
      const user = response?.data?.user;
      setAuth({user, accessToken})
      navigate(from, {replace:true});
    } catch (err) {
      if (!err.response) {
        setError(err.response.message)
      }
    }
  }
  const formik = useFormik({
    initialValues: {
      email: "",
      password: ""
    },
    validateOnBlur: true,
    onSubmit,
    validationSchema: validationSchema,
  });

  return (
    <div>
      <div className="grid grid-cols-12">
        <div className="col-start-6 col-span-3">
            <p className="mt-2 text-sm text-red-600 dark:text-red-500">
              { error ? error : '' }
            </p>
            <p className="mt-2 text-sm text-green-600 dark:text-green-500">
              { success ? success : '' }
            </p>
          <form onSubmit={formik.handleSubmit}>
            <div className="mb-6">
              <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                E-Posta adresinizi giriniz
              </label>
              <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                {formik.touched.email && formik.errors.email ? formik.errors.email : ''}
              </p>
              <input
                type="email"
                id="email"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder=""
                autoComplete={'off'}
                value={formik.values.email}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </div>
              <div className="mb-6">
                <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                  Şifreniz
                </label>
                <p className="mt-2 text-sm text-red-600 dark:text-red-500">
                  {formik.touched.password && formik.errors.password ? formik.errors.password : ''}
                </p>
                <input
                  type="password"
                  id="password"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  autoComplete={'off'}
                />
              </div>
              <button
                type="submit"
                className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                Giriş Yap
              </button>
              <a
                 className="text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md">
                <Link to={"/register"}>
                  Kayıt Ol
                </Link>
              </a>
          </form>
        </div>
      </div>
    </div>
  );
}

export default LoginForm;
